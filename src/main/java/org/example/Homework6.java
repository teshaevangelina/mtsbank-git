package org.example;

public class Homework6 {
    public static void main(String[] args) {
        Products firstProduct = new Products(1,11.0,0.75);
        Products secondProduct = new Products(1,7.0,42.575);
        Products thirdProduct = new Products(3,9.0,59.1);
        totalSum(firstProduct);
        totalSum(secondProduct);
        totalSum(thirdProduct);
    }

    public static void totalSum (Products product) {
        double amountWithDiscount = product.amountProducts * product.price * product.discount;
        double amountWithoutDiscount = product.amountProducts * product.price;
        System.out.println(Math.round(amountWithDiscount * 100.0) / 100.0);
        System.out.println(Math.round(amountWithoutDiscount * 100.0) / 100.0);
    }
}

 class Products {
    int amountProducts; // количество товаров
    double price; // цена товара
    double discount; //скидка

    public Products(int amountProducts, double price, double discount){
        this.amountProducts = amountProducts;
        this.price = price;
        this.discount = discount;
    }
}
